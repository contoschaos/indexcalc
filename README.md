# Readme

Little script to show how many times value of etf/index/... was UNDER and OVER average in month span.
Output is CSV file with:
1st. row is description.
2nd. row represents the number of times (from whole dataset) when a value was OVER average.
3rd. row represents the number of times (from whole dataset) when a value was UNDER average.

Data can be obtained from:
[www.nasdaq.com](https://www.nasdaq.com/market-activity/funds-and-etfs/spy/historical)

Selecting max range and clicking download.
Move it to data folder.
Call ```python3 calcV2.py <Name.csv>```

## More information

V2 was created, because older implementation based on months was biased.
V2 has its own problems, that it ignores first and last rows from dataset based on RANGE variable specified in script.
Please always look to results as ratio between OVER and UNDER average.
(29 - 31 day indexes are less common ?, because exchanges are open less number of times ? (and it is not scaled). TODO sanity check count.)

After playing with this more less i find out invest start month when it raises and end month when fall (or there is still a bias).
You can play with variables MAX, RANGE (removing data from dataset start, TODO add SKIP variable)
