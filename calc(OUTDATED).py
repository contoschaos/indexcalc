#!/usr/bin/env python
import csv
import sys

MAX = 420000  # only last <MAX> days
FILE = 'test.csv'

allMonths = []

if len(sys.argv) > 1:
    FILE = sys.argv[1]

print('Using file: data/', FILE)
with open('./data/%s' % FILE) as file:
    reader = csv.reader(file)
    # 32 position contains average of all from month
    # 33 position contains "second average" of all from month
    month = [0]*33
    dayCount = 0
    dayIndex = 0
    prevDayIndex = 0
    for dayRow in reader:
        if(dayCount > 23):
            print('ERROR: Unexpected data error. MORE than expected max trade days.')
        if(dayIndex > 31):
            print('ERROR: Unexpected data error. Day index out of range.')

        # Header skip
        if dayRow[0] == 'Date':
            continue

        if MAX <= 0:
            break
        MAX -= 1

        prevDayIndex = dayIndex
        dayIndex = int(dayRow[0][3:5])

        if(prevDayIndex < dayIndex and dayCount != 0):
            month[31] = month[31] / dayCount
            allMonths.append(month)
            month = [0]*33
            dayCount = 0

        if(dayRow[4] != '0' or dayRow[5] != '0'):
            value = round((float(dayRow[4])+float(dayRow[5]))/2, 4)
            month[dayIndex-1] = value

            dayCount += 1
            month[31] += value

    if(dayCount != 0):
        month[31] = month[31] / dayCount
        allMonths.append(month)

lowerDays = [0]*31
underAvgSum = 0

print('Number of times value was lower than month average.')
for month in allMonths:
    dayCount = 0
    for i, dayValue in enumerate(month):
        if(dayValue != 0 and dayValue < month[31]):
            lowerDays[i] += 1
            underAvgSum += dayValue
            dayCount += 1
    secondAverage = 0
    if(dayCount != 0):
        secondAverage = underAvgSum/dayCount
    month[32] = secondAverage
    underAvgSum = 0
print(*lowerDays, sep=',')

print('Last ', len(allMonths), 'months were included')
print('Number of times value was lower than "second" average.')
lowest = [0]*31
for month in allMonths:
    for i, dayValue in enumerate(month):
        if(dayValue != 0 and month[32] != 0 and dayValue < month[32]):
            lowest[i] += 1
print(*lowest, sep=',')


with open("result.csv", 'w') as resultFile:
    writer = csv.writer(resultFile)
    writer.writerow(
        ['Columns represent a day of month, its value represents how many times the value of day was under each month average. (second row represents 2nd time average)'])
    writer.writerow(lowerDays)
    writer.writerow(lowest)
