#!/usr/bin/env python
import csv
import sys
import numpy as np
from functools import reduce

MAX = 42042042  # only last <MAX> days
RANGE = 15  # Have to be odd number, ! Beware first and last <RANGE> rows will be ignored !

FULL_RANGE = RANGE * 2 + 1
FILE = 'test.csv'
allDataLess = [0]*31
allDataMore = [0]*31  # + and same as average


def avgFunc(a, b):
    return a+b[1]


if len(sys.argv) > 1:
    FILE = sys.argv[1]

print('Using file: data/', FILE)
with open('./data/%s' % FILE) as file:
    reader = csv.reader(file)
    buffer = []
    for dayRow in reader:
        # Header skip
        if dayRow[0] == 'Date':
            continue

        if MAX <= 0:
            break
        MAX -= 1

        dayIndex = int(dayRow[0][3:5])
        if(dayRow[4] != '0' or dayRow[5] != '0'):
            value = round((float(dayRow[4])+float(dayRow[5]))/2, 4)

            buffer.append((dayIndex, value))  # FIFO in
            if(len(buffer) == FULL_RANGE):
                # init accumulator, done to not ask in each iteration if first time is float or tuple
                buffer.insert(0, 0.0)
                avg = reduce(avgFunc, buffer)/FULL_RANGE
                buffer.pop(0)  # remove initializer
                if(buffer[RANGE][1] < avg):
                    allDataLess[buffer[RANGE][0]-1] += 1
                else:
                    allDataMore[buffer[RANGE][0]-1] += 1
                buffer.pop(0)  # FIFO out

with open("result.csv", 'w') as resultFile:
    writer = csv.writer(resultFile)
    writer.writerow(
        ['Columns represent a day of month. A2 row values represent how many times the value of day was over/same each month average. A3 row values represent how many times the value of day was under each month average.'])
    writer.writerow(allDataMore)
    writer.writerow(allDataLess)

print('Results in : result.csv')
